import requests

from django.db import transaction
from django.shortcuts import render
from django.views.decorators.http import require_GET
from rest_framework import status

from cards import const
from cards.lib import save_image_from_url
from cards.models import Card, Profile


@require_GET
@transaction.atomic
def buy_card(request):
    """Купить карту"""
    template = 'home.html'

    card_name = request.GET.get('q', None)
    if not card_name:
        return render(
            request, template,
            {'error_message': const.EMPTY_QUERY_MSG}
        )

    card_name = card_name.lower()
    profile = Profile.objects.select_for_update().get(user__id=request.user.id)
    card = Card.objects.filter(name=card_name)\
        .prefetch_related('owners').first()

    # Если баланс пользователя меньше цены и еще не владеет этой картой
    if profile.balance < const.CARD_PRICE \
            and not (card and profile in card.owners.all()):
        return render(
            request, template,
            {'error_message': const.NOT_ENOUGH_MONEY_MSG}
        )

    if not card:
        r = requests.get(
            const.CARDS_SEARCH_API_URL,
            params={'q': '!{}'.format(card_name.replace(' ', '_'))}
        )
        if r.status_code == status.HTTP_404_NOT_FOUND:
            return render(request, template,
                          {'error_message': const.CARD_NOT_FOUND_MSG})

        image_uri = r.json()['data'][0]['image_uris']['normal']
        card = Card(name=card_name)
        save_image_from_url(card, image_uri, card_name)

    if profile not in card.owners.all():
        profile.balance -= const.CARD_PRICE
        profile.save(update_fields=['balance'])
        card.owners.add(profile)

    template = 'bought_card.html'
    context = {
        'image_url': card.img.url,
        'card_name': card_name.capitalize()
    }
    return render(request, template, context)
