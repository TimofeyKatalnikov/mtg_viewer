CARDS_SEARCH_API_URL = 'https://api.scryfall.com/cards/search'

CARD_PRICE = 30

EMPTY_QUERY_MSG = 'Please, input some data!'
CARD_NOT_FOUND_MSG = 'Card with this name not found'
NOT_ENOUGH_MONEY_MSG = 'You don\'t have enough money to buy new card'
