from django.urls import path

from cards.views import buy_card


urlpatterns = [
    path('buy', buy_card),
]
