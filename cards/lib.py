import requests

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile


def save_image_from_url(card, url, name):
    """
    Сохранить jpg-изображение, расположенное по адресу url, под именем name
    и добавить в поле img объекта карты

    :param card: Объект модели cards.Card
    :type card: cards.Card object
    :param url: Адрес изображения
    :type url: str
    :param name: Имя для сохранения
    :type name: str
    """
    r = requests.get(url)

    img_temp = NamedTemporaryFile(delete=True)
    img_temp.write(r.content)
    img_temp.flush()

    card.img.save('{}.jpg'.format(name), File(img_temp), save=True)
