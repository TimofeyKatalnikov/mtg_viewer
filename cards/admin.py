from django.contrib import admin

from cards.models import Profile, Card


admin.site.register(Profile)
admin.site.register(Card)
