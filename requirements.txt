Django==2.1.2
psycopg2==2.7.5
psycopg2-binary==2.7.5
requests==2.19.1
djangorestframework==3.8.2